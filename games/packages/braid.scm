;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages braid)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages fltk)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages xorg)
  #:use-module (nongnu packages game-development)
  #:use-module (games humble-bundle))

(define-public braid
  (package
    (name "braid")
    (version "2015-06-11")
    (source
     (origin
       (method humble-bundle-fetch)
       (uri (humble-bundle-reference
             (help (humble-bundle-help-message name))
             (config-path (list (string->symbol name) 'key))
             ;; Alternative: "braid-linux-build2.run.bin".  The .bin version
             ;; includes a 64-bit build but seems to be older and rely on SDL1.
             (files (list (string-append "BraidSetup-" version ".sh") ))))
       (file-name (string-append "BraidSetup-" version ".sh"))
       (sha256
        (base32
         "15sxcw8d55xkdibklcr5yz8cy6g4ifxsazlj4xlzyda13n7sqp9s"))))
    (build-system binary-build-system)
    (supported-systems '("i686-linux" "x86_64-linux"))
    (arguments
     `(#:strip-binaries? #f
       #:system "i686-linux"
       #:patchelf-plan
       `(("data/x86/Braid.bin.x86"
          ("libc" "mesa" "gcc:lib" "sdl2" "nvidia-cg-toolkit"))
         ("data/x86/launcher.bin.x86"
          ("libc" "libxcursor" "libxfixes" "libxext" "libxft" "fontconfig" "libxinerama"
           "libx11" "fltk" "sdl2" "gcc:lib")))
       #:install-plan
       `(("data/noarch" (".") "share/braid/")
         ("data/x86" ("Braid.bin.x86" "launcher.bin.x86") "share/braid/"))
       #:phases
       (modify-phases %standard-phases
         (replace 'unpack
           (lambda* (#:key inputs #:allow-other-keys)
             (invoke (which "makeself_safeextract")
                     "--mojo"
                     (assoc-ref inputs "source"))
             (chdir ((@@ (guix build gnu-build-system) first-subdirectory) "."))
             #t))
         (add-after 'install 'make-wrapper
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((output (assoc-ref outputs "out"))
                    (wrapper (string-append output "/bin/braid"))
                    (launcher (string-append output "/bin/braid-launcher"))
                    (real (string-append output "/share/braid/Braid.bin.x86"))
                    (real-launcher (string-append output "/share/braid/launcher.bin.x86"))
                    (icon (string-append output "/share/braid/Icon.png")))
               (mkdir-p (dirname launcher))
               (call-with-output-file launcher
                 (lambda (p)
                   (format p
                           (string-append
                            "#!" (assoc-ref inputs "bash") "/bin/bash" "\n"
                            "cd " output "/share/braid" "\n"
                            "exec -a " (basename launcher) " " real-launcher " \"$@\"" "\n"))))
               (chmod launcher #o755)
               (call-with-output-file wrapper
                 (lambda (p)
                   (format p
                           (string-append
                            "#!" (assoc-ref inputs "bash") "/bin/bash" "\n"
                            "cd " output "/share/braid" "\n"
                            ;; Warning: Possibly due to a bug in the game or in
                            ;; Guix' FLTK, we need to disable the launcher.
                            ;; Otherwise we could also start the game by holding
                            ;; the Shift key (directly or from the launcher).
                            "exec -a " (basename wrapper) " " real " -no_launcher \"$@\"" "\n"))))
               (chmod wrapper #o755)
               (make-desktop-entry-file (string-append output "/share/applications/braid.desktop")
                                        #:name "Braid"
                                        #:exec wrapper
                                        #:icon icon
                                        #:categories '("Application" "Game")))
             #t)))))
    (native-inputs
     `(("makeself-safeextract" ,makeself-safeextract)))
    (inputs
     `(("gcc:lib" ,gcc "lib")
       ("mesa" ,mesa)
       ("sdl2" ,sdl2)
       ("fltk" ,fltk)
       ("libxcursor" ,libxcursor)
       ("libxfixes" ,libxfixes)
       ("libxext" ,libxext)
       ("libxft" ,libxft)
       ("fontconfig" ,fontconfig)
       ("libxinerama" ,libxinerama)
       ("libx11" ,libx11)
       ("nvidia-cg-toolkit" ,nvidia-cg-toolkit)))
    (home-page "http://braid-game.com/")
    (synopsis "Platform puzzle game where you manipulate the flow of time")
    (description
     "Braid is a platform and puzzle video game.
The basic story elements in Braid unfold as the protagonist, Tim, attempts to
rescue a princess from a monster.  Text passages laid throughout the game reveal
a multifaceted narrative, giving clues about Tim's contemplations and
motivations.  The game features traditionally defining aspects of the platform
genre while also integrating various novel powers of time-manipulation.  Using
these abilities, the player progresses through the game by finding and
assembling jigsaw puzzle pieces.

Warning: Due to a bug in the game, the launcher might not be able to start the
game if you don't hold this Shift key.  Otherwise start the \"braid\" executable
directly.")
    (license (undistributable "No URL"))))
