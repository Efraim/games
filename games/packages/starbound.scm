;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages starbound)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages sdl)
  #:use-module (games humble-bundle))

(define-public starbound
  (let* ((version "1.4.4")
         (file-name (string-append "starbound_" version "_linux.zip")))
    (package
      (name "starbound")
      (version version)
      (source
       (origin
         (method humble-bundle-fetch)
         (uri (humble-bundle-reference
               (help (humble-bundle-help-message name))
               (config-path (list (string->symbol name) 'key))
               (files (list file-name))))
         (file-name file-name)
         (sha256
          (base32
           "1x24662awr6a3j1lxawcqmn6xqwg7h5v4zvc3mzvxn52k9winmws"))))
      (build-system binary-build-system)
      (supported-systems '("x86_64-linux"))
      (arguments
       `(#:patchelf-plan
         '(("linux/asset_packer")
           ("linux/asset_unpacker")
           ("linux/dump_versioned_json")
           ("linux/libsteam_api.so" ("gcc"))
           ("linux/make_versioned_json")
           ("linux/planet_mapgen")
           ("linux/starbound" ("glu" "mesa" "sdl2" "out"))
           ("linux/starbound_server"))
         #:install-plan
         `(("assets" "share/starbound/assets")
           ("doc" "share/starbound/doc")
           ("linux/libsteam_api.so" "lib/")
           ("linux" "share/starbound/linux" #:exclude ("libsteam_api.so" "sbinit.config"))
           ("tiled" "share/starbound/tiled"))
         #:validate-runpath? #f    ; TODO: libsteam_api.so does not pass, why?
         #:phases
         (modify-phases %standard-phases
           (add-after 'install 'create-wrapper
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (wrapper (string-append output "/bin/starbound"))
                      (real (string-append output "/share/starbound/linux/starbound")))
                 (mkdir-p (dirname wrapper))
                 ;; The game assumes its own directory to be writable.
                 ;; We tell it to use a different "storage" directory by
                 ;; specifying an alternate sbinit.config file.  This is
                 ;; necessary because sbinit.config does not understand "~"
                 ;; nor $HOME.  The parent directory of "storage" must exist.
                 (call-with-output-file wrapper
                   (lambda (p)
                     (format p
                             (string-append
                              "#!" (assoc-ref inputs "bash") "/bin/bash" "\n"
                              "[ -n \"$XDG_DATA_HOME\" ] || XDG_DATA_HOME=\"$HOME/.local/share\"" "\n"
                              "mkdir -p \"$XDG_DATA_HOME/starbound\"" "\n"
                              "if [ ! -f \"$XDG_DATA_HOME/starbound/sbinit.config\" ]; then" "\n"
                              "  cat<<EOF>\"$XDG_DATA_HOME/starbound/sbinit.config\"" "\n"
                              "{
  \"assetDirectories\" : [
    \"../assets/\",
    \"$XDG_DATA_HOME/starbound/mods/\"
  ],

  \"storageDirectory\" : \"$XDG_DATA_HOME/starbound/storage/\"
}" "\n"

                              "EOF" "\n"
                              "fi" "\n"
                              "
# Starbound aborts if an argument is specified twice.
hasBootconfigArg() {
  while [ $# -gt 0 ]; do
    if [ \"${1#-}\" != \"$1\" ]; then
      case \"${1#-}\" in
        bootconfig) return 0;;
        # Arguments that expect a parameter
        loglevel|logfile|runtimeconfig) shift;;
      esac
    fi
    shift
  done
  return 1
}" "\n"
                              "if hasBootconfigArg \"$@\"; then" "\n"
                              "  exec -a " (basename real) " " real " \"$@\"" "\n"
                              "else" "\n"
                              "  exec -a " (basename real) " " real
                              " -bootconfig " "\"$XDG_DATA_HOME/starbound/sbinit.config\""
                              " \"$@\"" "\n"
                              "fi" "\n"))))
                 (chmod wrapper #o755)
                 (make-desktop-entry-file (string-append output "/share/applications/starbound.desktop")
                                          #:name "Worms Reloaded"
                                          #:exec wrapper
                                          ;; #:icon icon ; TODO: No icon?
                                          #:comment "An extraterrestrial sandbox adventure game"
                                          #:categories '("Application" "Game")))
               #t))
           (add-after 'install 'make-executable
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (data (string-append output "/share/starbound/linux")))
                 (chdir data)
                 (for-each (lambda (f)
                             (chmod f #o755))
                           '("asset_packer" "asset_unpacker" "dump_versioned_json"
                             "make_versioned_json" "planet_mapgen"
                             "starbound" "starbound_server")))
               #t)))))
      (native-inputs
       `(("unzip" ,unzip)))
      (inputs
       `(("gcc" ,gcc "lib")
         ("mesa" ,mesa)
         ("glu" ,glu)
         ("sdl2" ,sdl2)))
      (home-page "https://playstarbound.com/")
      (synopsis "Extraterrestrial sandbox adventure game")
      (description "You’ve fled your home, only to find yourself lost in space
with a damaged ship.  Your only option is to beam down to the planet below and
gather the resources you need to repair your ship and set off to explore the
vast, infinite universe…

In Starbound, you create your own story - there's no wrong way to play!  You
may choose to save the universe from the forces that destroyed your home,
uncovering greater galactic mysteries in the process, or you may wish to
forego a heroic journey entirely in favor of colonizing uncharted planets.")
      (license (undistributable "No URL")))))
